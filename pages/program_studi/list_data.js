import Image from 'next/image'
import Link from 'next/link'
import Router from 'next/router'

import logoprodi from '../../assets/logo_prodi.png'
import iconstar from '../../assets/Star.svg'
import useSWR from 'swr'
import axios from 'axios'
import { useRouter } from 'next/router';
export default function ListData() {
    

  
    const router = useRouter()

  const fetcher = url => fetch(url).then(r => r.json())
  const { data, error } = useSWR('http://127.0.0.1:8000/api/prodi', fetcher)

  if (error) return <div>failed to load</div>
  if (!data) return <div>loading...</div>
  const deleteProdi = async (id) => {

    //sending
    await axios.delete(`http://127.0.0.1:8000/api/prodi/${id}`);

router.reload(window.location.pathname)
}

  return (
    <div >
      
  {
          data.data.map((data)=>{
            return(
  <div className='border' style={{ height:'100px',borderWidth:'1px', borderRadius:'8px', borderColor:'black', display:'flex', marginBottom:'24px',alignItems:'center',paddingLeft:'16px'}}>
    <div>
    <Image
          src={logoprodi}
          width={68}
          height={68}
        />
    </div>
    <div style={{display:'flex',justifyContent:'space-between',width:'100%'}}>
    <div style={{marginLeft:'16px'}}>
      <div style={{display:'flex',flexDirection:'column', justifyContent:'start'}}>
      <font style={{color:'#172426', fontWeight:'bold',fontSize:'16px'}}>{data.nama_prodi}</font>
      <div>
      <Image
          src={iconstar}
          width={14}
          height={14}
        />
       Akreditasi {data.akreditasi_prodi}
      </div>
      <p>
      {data.deskripsi_prodi}
      </p>
      </div>
      <div>
        
      </div>
    </div>
    <div style={{display:'flex',justifyContent:'center',alignItems:'center',padding:'16px'}}>
    <Link href={`/program_studi/update?id=${data.id}`}>
    <div style={{width:40, height:40, background:'#F5F6F7', display:'flex', justifyContent:'center',alignItems:'center', borderRadius:20, marginRight:'8px'}}>
                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M11.4562 17.0357H17.5" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M10.65 3.16233C11.2964 2.38982 12.4583 2.27655 13.2469 2.90978C13.2905 2.94413 14.6912 4.03232 14.6912 4.03232C15.5575 4.55599 15.8266 5.66925 15.2912 6.51882C15.2627 6.56432 7.34329 16.4704 7.34329 16.4704C7.07981 16.7991 6.67986 16.9931 6.25242 16.9978L3.21961 17.0358L2.53628 14.1436C2.44055 13.7369 2.53628 13.3098 2.79975 12.9811L10.65 3.16233Z" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M9.18402 5.00073L13.7276 8.49" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg>

                </div>
                </Link>
                <button onClick={() => deleteProdi(data.id)} style={{width:40, height:40, background:'#F5F6F7', display:'flex', justifyContent:'center',alignItems:'center', borderRadius:20,borderWidth:0}}>
                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M16.1041 7.89017C16.1041 7.89017 15.6516 13.5027 15.3891 15.8668C15.2641 16.996 14.5666 17.6577 13.4241 17.6785C11.2499 17.7177 9.07326 17.7202 6.89993 17.6743C5.80076 17.6518 5.11493 16.9818 4.99243 15.8727C4.72826 13.4877 4.27826 7.89017 4.27826 7.89017" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M17.2569 5.19975H3.12518" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M14.5339 5.19975C13.8797 5.19975 13.3164 4.73725 13.188 4.09642L12.9855 3.08308C12.8605 2.61558 12.4372 2.29225 11.9547 2.29225H8.42719C7.94469 2.29225 7.52136 2.61558 7.39636 3.08308L7.19386 4.09642C7.06552 4.73725 6.50219 5.19975 5.84802 5.19975" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg>


                </button>
      </div>
    </div>
    <div>

    </div>
  </div>
            )})
}
  </div>
  )
}
