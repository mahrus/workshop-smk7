import Header from '../komponen/header'
import Sidebar from '../komponen/sidebar'
import Layout from '../komponen/layout'
import ListData from './list_data'
import Link from 'next/link'

export default function Home() {

    return (
        <div>
            <Layout>
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3" >
                    <h1 style={{ fontSize: '20px', color: '#172426' }}>Program Studi</h1>
                    <div className="btn-toolbar mb-2 mb-md-0">
                        <Link href="/program_studi/tambah">
                            <button type="button" className="btn btn-sm btn-outline-secondary">
                                Tambah Data
                            </button>
                        </Link>
                    </div>
                </div>
                <ListData />
            </Layout>
        </div>
    )
}
