import Layout from '../komponen/layout'
import React, { useState } from 'react';
import useSWR from 'swr'
import Router from 'next/router';

import { useRouter } from 'next/router'
export default function Update() {
    const router = useRouter()
  const { id } = router.query
  
  const fetcher = url => fetch(url).then(r => r.json())
  const { data, error } = useSWR('http://127.0.0.1:8000/api/prodi/'+id, fetcher)

  if (error) return <div>failed to load</div>
  if (!data) return <div>loading...</div>
  //state
  const [nama, setNama] = useState(data.data.nama_prodi);
  const [akreditasi, setAkreditasi] = useState(data.data.akreditasi_prodi);
  const [deskripsi, setDeskripsi] = useState(data.data.deskripsi_prodi);
//   const nama=data.data.nama_prodi
//   const akreditasi=data.data.akreditasi_prodi
//   const deskripsi=data.data.deskripsi_prodi
  const storePost = async (e) => {
    e.preventDefault();
    const res = await fetch('http://127.0.0.1:8000/api/prodi/'+id, {
        method: 'POST',
        body: JSON.stringify({ nama_prodi: nama, akreditasi_prodi:akreditasi, deskripsi_prodi:deskripsi }),
        headers: {
          'Content-Type': 'application/json',
        },
      })
      const data = await res.json()
      if(data.status){
        Router.push('/program_studi')
      }
        
  }
    return (
        <div >
            <Layout>
                <div style={{ backgroundColor: 'white', padding: '24px', borderRadius: '8px' }}>
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3" >
                        <h1 style={{ fontSize: '20px', color: '#172426' }}>Tambah Program Studi</h1>
                        <div className="btn-toolbar mb-2 mb-md-0">
                            <button type="button" className="btn btn-sm btn-outline-secondary">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali
                            </button>
                            &nbsp;
                            &nbsp;
                           
                        </div>
                    </div>
                    <div>
                        <form onSubmit={storePost}>
                            <div className="row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="inputEmail4" style={{ fontSize: '14px', fontWeight: 'normal', marginBottom: '8px' }}>Nama Program Studi</label>
                                    <input type="text" className="form-control" id="inputEmail4" placeholder="Program Studi" value={nama} onChange={(e) => setNama(e.target.value)}/>
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="inputPassword4" style={{ fontSize: '14px', fontWeight: 'normal', marginBottom: '8px'  }} >Akreditasi {akreditasi}</label>
                                    <select id="inputState" class="form-control" value={akreditasi} onChange={(e) => setAkreditasi(e.target.value)}>
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="C">C</option>
                                    </select>
                                </div>
                                <div className="form-group col-md-12" style={{ marginTop: '8px' }}>
                                    <label htmlFor="inputPassword4" style={{ fontSize: '14px', fontWeight: 'normal', marginBottom: '8px' }} >Deskripsi</label>
                                    <textarea class="form-control" value={deskripsi} onChange={(e) => setDeskripsi(e.target.value)}>
                                    </textarea>
                                </div>
                            </div>
                            <button type="submit" className="btn btn-sm btn-primary" style={{marginTop:'8px'}} 
                            >
                             Simpan
                            </button>
                        </form>
                    </div>




                </div>
            </Layout>

        </div>
    )
}
