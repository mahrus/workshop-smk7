

export default function Sidebar() {
  return (
    <nav id="sidebarMenu" className="col-md-3 col-lg-2 d-md-block sidebar collapse" style={{ backgroundColor: 'white' }}>
      <div className="position-sticky pt-3">
        <ul className="nav flex-column">
          <li className="nav-item" style={{ display: 'flex', alignItems: 'center', alignContent: 'center' }}>
            <a className="nav-link" aria-current="page" href="#" style={{ color: '#718284' }}>
              <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M1 4.5C1 1.87479 1.02811 1 4.5 1C7.97189 1 8 1.87479 8 4.5C8 7.12521 8.01107 8 4.5 8C0.988927 8 1 7.12521 1 4.5Z" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                <path fill-rule="evenodd" clip-rule="evenodd" d="M12 4.5C12 1.87479 12.0281 1 15.5 1C18.9719 1 19 1.87479 19 4.5C19 7.12521 19.0111 8 15.5 8C11.9889 8 12 7.12521 12 4.5Z" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                <path fill-rule="evenodd" clip-rule="evenodd" d="M1 15.5C1 12.8748 1.02811 12 4.5 12C7.97189 12 8 12.8748 8 15.5C8 18.1252 8.01107 19 4.5 19C0.988927 19 1 18.1252 1 15.5Z" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                <path fill-rule="evenodd" clip-rule="evenodd" d="M12 15.5C12 12.8748 12.0281 12 15.5 12C18.9719 12 19 12.8748 19 15.5C19 18.1252 19.0111 19 15.5 19C11.9889 19 12 18.1252 12 15.5Z" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
              </svg>


              <font style={{ marginLeft: '15px', fontSize: '14px' }}>Home</font>
            </a>
          </li>
          <li className="nav-item" style={{ display: 'flex', alignItems: 'center', alignContent: 'center', backgroundColor: '#E5EBEA', borderRadius: 8 }}>
            <a className="nav-link" aria-current="page" href="#" style={{ color: '#185951' }}>
              <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M8.07874 15.1354H13.8937" stroke="#185951" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                <path fill-rule="evenodd" clip-rule="evenodd" d="M1.40002 12.713C1.40002 7.082 2.01402 7.475 5.31902 4.41C6.76502 3.246 9.01502 1 10.958 1C12.9 1 15.195 3.235 16.654 4.41C19.959 7.475 20.572 7.082 20.572 12.713C20.572 21 18.613 21 10.986 21C3.35903 21 1.40002 21 1.40002 12.713Z" stroke="#185951" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
              </svg>

              <font style={{ marginLeft: '15px', fontSize: '14px' }}>Program Studi</font>
            </a>
          </li>
          <li className="nav-item" style={{ display: 'flex', alignItems: 'center', alignContent: 'center' }}>
            <a className="nav-link" aria-current="page" href="#" style={{ color: '#718284' }}>
              <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M15.7162 16.2234H8.49622" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                <path d="M15.7162 12.0369H8.49622" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                <path d="M11.2513 7.86008H8.49634" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                <path fill-rule="evenodd" clip-rule="evenodd" d="M15.9086 2.74979C15.9086 2.74979 8.23161 2.75379 8.21961 2.75379C5.45961 2.77079 3.75061 4.58679 3.75061 7.35679V16.5528C3.75061 19.3368 5.47261 21.1598 8.25661 21.1598C8.25661 21.1598 15.9326 21.1568 15.9456 21.1568C18.7056 21.1398 20.4156 19.3228 20.4156 16.5528V7.35679C20.4156 4.57279 18.6926 2.74979 15.9086 2.74979Z" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
              </svg>
              <font style={{ marginLeft: '15px', fontSize: '14px' }}>Blog</font>
            </a>
          </li>
          <li className="nav-item" style={{ display: 'flex', alignItems: 'center', alignContent: 'center' }}>
            <a className="nav-link" aria-current="page" href="#" style={{ color: '#718284' }}>
              <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M13.1043 4.17701L14.9317 7.82776C15.1108 8.18616 15.4565 8.43467 15.8573 8.49218L19.9453 9.08062C20.9554 9.22644 21.3573 10.4505 20.6263 11.1519L17.6702 13.9924C17.3797 14.2718 17.2474 14.6733 17.3162 15.0676L18.0138 19.0778C18.1856 20.0698 17.1298 20.8267 16.227 20.3574L12.5732 18.4627C12.215 18.2768 11.786 18.2768 11.4268 18.4627L7.773 20.3574C6.87023 20.8267 5.81439 20.0698 5.98724 19.0778L6.68385 15.0676C6.75257 14.6733 6.62033 14.2718 6.32982 13.9924L3.37368 11.1519C2.64272 10.4505 3.04464 9.22644 4.05466 9.08062L8.14265 8.49218C8.54354 8.43467 8.89028 8.18616 9.06937 7.82776L10.8957 4.17701C11.3477 3.27433 12.6523 3.27433 13.1043 4.17701Z" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
              </svg>
              <font style={{ marginLeft: '15px', fontSize: '14px' }}>Event</font>
            </a>
          </li>
          <li className="nav-item" style={{ display: 'flex', alignItems: 'center', alignContent: 'center' }}>
            <a className="nav-link" aria-current="page" href="#" style={{ color: '#718284' }}>
              <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M15.8928 15.8915C13.346 18.4386 9.57483 18.9889 6.4887 17.5617C6.03311 17.3782 5.65959 17.23 5.3045 17.23C4.31543 17.2359 3.08433 18.1949 2.44449 17.5558C1.80465 16.9159 2.76441 15.6838 2.76441 14.6888C2.76441 14.3336 2.62203 13.9668 2.43862 13.5103C1.01071 10.4247 1.56178 6.65223 4.10857 4.106C7.35969 0.853683 12.6417 0.853683 15.8928 4.10516C19.1498 7.3625 19.1439 12.6401 15.8928 15.8915Z" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                <path d="M13.2829 10.3442H13.2904" stroke="#718284" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                <path d="M9.94203 10.3442H9.94953" stroke="#718284" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                <path d="M6.60121 10.3442H6.60871" stroke="#718284" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
              </svg>
              <font style={{ marginLeft: '15px', fontSize: '14px' }}>Class Discussion</font>
            </a>
          </li>
          <li className="nav-item" style={{ display: 'flex', alignItems: 'center', alignContent: 'center' }}>
            <a className="nav-link" aria-current="page" href="#" style={{ color: '#718284' }}>
              <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M2.87187 11.5983C1.79887 8.24832 3.05287 4.41932 6.56987 3.28632C8.41987 2.68932 10.4619 3.04132 11.9999 4.19832C13.4549 3.07332 15.5719 2.69332 17.4199 3.28632C20.9369 4.41932 22.1989 8.24832 21.1269 11.5983C19.4569 16.9083 11.9999 20.9983 11.9999 20.9983C11.9999 20.9983 4.59787 16.9703 2.87187 11.5983Z" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                <path d="M16 6.7C17.07 7.046 17.826 8.001 17.917 9.122" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
              </svg>






              <font style={{ marginLeft: '15px', fontSize: '14px' }}>Testimonials</font>
            </a>
          </li>
          <li className="nav-item" style={{ display: 'flex', alignItems: 'center', alignContent: 'center' }}>
            <a className="nav-link" aria-current="page" href="#" style={{ color: '#718284' }}>
              <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M20.8067 7.62357L20.1842 6.54348C19.6577 5.62956 18.4907 5.31427 17.5755 5.83867V5.83867C17.1399 6.0953 16.6201 6.16811 16.1307 6.04104C15.6413 5.91398 15.2226 5.59747 14.9668 5.16133C14.8023 4.8841 14.7139 4.56835 14.7105 4.24599V4.24599C14.7254 3.72918 14.5304 3.22836 14.17 2.85762C13.8096 2.48689 13.3145 2.27782 12.7975 2.27803H11.5435C11.037 2.27802 10.5513 2.47987 10.194 2.8389C9.83669 3.19793 9.63717 3.68455 9.63961 4.19107V4.19107C9.6246 5.23688 8.77248 6.07677 7.72657 6.07666C7.40421 6.07331 7.08846 5.9849 6.81123 5.82036V5.82036C5.89606 5.29597 4.72911 5.61125 4.20254 6.52517L3.53435 7.62357C3.00841 8.53635 3.3194 9.70256 4.23 10.2323V10.2323C4.8219 10.574 5.18653 11.2055 5.18653 11.889C5.18653 12.5725 4.8219 13.204 4.23 13.5458V13.5458C3.32056 14.0719 3.00923 15.2353 3.53435 16.1453V16.1453L4.16593 17.2346C4.41265 17.6797 4.8266 18.0082 5.31619 18.1474C5.80578 18.2865 6.33064 18.2249 6.77462 17.976V17.976C7.21108 17.7213 7.73119 17.6515 8.21934 17.7822C8.70749 17.9128 9.12324 18.233 9.37416 18.6716C9.5387 18.9488 9.62711 19.2646 9.63046 19.587V19.587C9.63046 20.6435 10.487 21.5 11.5435 21.5H12.7975C13.8505 21.5 14.7055 20.6491 14.7105 19.5961V19.5961C14.7081 19.088 14.9089 18.6 15.2682 18.2407C15.6275 17.8814 16.1155 17.6806 16.6236 17.6831C16.9452 17.6917 17.2596 17.7797 17.5389 17.9394V17.9394C18.4517 18.4653 19.6179 18.1543 20.1476 17.2437V17.2437L20.8067 16.1453C21.0618 15.7075 21.1318 15.186 21.0012 14.6963C20.8706 14.2067 20.5502 13.7893 20.111 13.5366V13.5366C19.6718 13.2839 19.3514 12.8665 19.2208 12.3769C19.0902 11.8873 19.1603 11.3658 19.4154 10.9279C19.5812 10.6383 19.8214 10.3982 20.111 10.2323V10.2323C21.0161 9.70285 21.3264 8.54345 20.8067 7.63272V7.63272V7.62357Z" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                <circle cx="12.1751" cy="11.889" r="2.63616" stroke="#718284" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
              </svg>
              <font style={{ marginLeft: '15px', fontSize: '14px' }}>Settings</font>
            </a>
          </li>
        </ul>

      </div>
    </nav>
  )

}